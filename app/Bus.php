<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    protected $fillable=[ 'bus_name','capacity'];

    public function routes()
    {
        return $this->belongsToMany('App\Route');
    }
}
