<?php

namespace App\Http\Controllers;

use App\Fare;
use App\Http\Resources\FareCollection as FareResource;
use Illuminate\Http\Request;

class FareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fares = Fare::orderBy('date')->get();
        return view('fare.index',compact('fares'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fare = new Fare();
        return view('fare.create', compact('fare'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'start_id' => 'required',
            'destination_id' => 'required',
        ]);
        $fare = new Fare();
        $fare->fill($request->only('start_id','destination_id','fare_amount'));
//        $fare->date = date_format($request->input('date'), 'Y-m-d');
        $fare->date = date("Y-m-d", strtotime($request->input('date')));

        $fare->save();
        return redirect()->route('fares.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function api(Request $request)
    {
        $data = array();
        $date = date("Y-m-d", strtotime($request->input('date')));
        $fares = Fare::where('date',$date)->paginate();
        foreach( $fares as $fare){
            $fare->start_name = \App\Stops::find($fare->start_id)->stop_name;
            $fare->destination_name = \App\Stops::find($fare->destination_id)->stop_name;
        }

//        return response()->json($fares, 200);
        return new FareResource($fares);
    }
}
