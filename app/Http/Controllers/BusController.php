<?php

namespace App\Http\Controllers;

use App\Bus;
use Illuminate\Http\Request;
use Illuminate\View\View;

class BusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_buses = Bus::all();
        $bus = new Bus();
        return view('bus.index', compact('all_buses','bus'));
    }


    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'bus_name' => 'required|max:255|min:1',
        ]);
        $bus = new Bus();
        $bus->fill($request->only('bus_name','capacity'));
        $bus->save();
        return redirect()->route('buses.index');
    }



    public function edit($id)
    {
       $bus = Bus::find($id);
       return view('bus.edit', compact('bus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'bus_name' => 'required|max:255|min:1',
        ]);
        $bus = Bus::find($id);
        $bus->fill($request->only('bus_name','capacity'));
        $bus->update();

        return redirect()->route('buses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
