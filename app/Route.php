<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable=[];

    public function buses()
    {
        return $this->belongsToMany('App\Bus');
    }
    public function stops()
    {
        return $this->belongsToMany('App\Stops');
    }
}
