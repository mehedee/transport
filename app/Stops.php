<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stops extends Model
{
    protected $fillable=['stop_name','description','time_from_prev','image'];

    public function routes()
    {
        return $this->belongsToMany('App\Route');
    }
}
