<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fare extends Model
{
    protected $fillable=['start_id','destination_id','date','fare_amount'];
}
