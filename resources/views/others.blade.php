@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <form action="{{route('fare-by-day')}}" method="GET">
                    <div class="form-group">
                        <input type="date" class="form-control" name="date">

                    </div>
                    <button type="submit" class="btn btn-primary">Check Response</button>
                </form>
            </div>
        </div>
    </div>

    @endsection
