@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3>Update :</h3>
                <form method="post" action="{{ route('stops.update', $stop->id)}}" >
                    @csrf()
                    {{ method_field('PUT')}}
                    @include('stops.form')
                </form>
            </div>
        </div>
    </div>
@endsection
