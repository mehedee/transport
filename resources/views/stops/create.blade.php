@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h3>Add New Stoppage</h3>
                <hr>
                <form action="{{ route('stops.store') }}" method="post">
                    @csrf()
                    @include('stops.form')
                </form>
            </div>
        </div>

    </div>
@endsection
