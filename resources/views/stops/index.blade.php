@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3>List of Stops:</h3>
                <hr>
                <a href="{{ route('stops.create') }}">Add new Stop</a>
                <table class="table table-bordered">
                    <tr>
                        <th>Stoppage Name</th>
                        <th>Description</th>
                    </tr>
                    @foreach( $all_stops as $stop)
                        <tr>
                            <td>
                                {{ $stop->stop_name }} <br>
                                ( <a href="{{ Route('stops.edit', $stop->id ) }}" class="small">Update </a> )
                            </td>
                            <td>
                                {!! $stop->description !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

    </div>
@endsection
