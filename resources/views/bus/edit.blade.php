@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3>Update Bus Name & Capacity:</h3>
                <form method="post" action="{{ route('buses.update', $bus->id)}}" >
                    @csrf()
                    {{ method_field('PUT')}}
                    @include('bus.form')
                </form>
            </div>
        </div>
    </div>
@endsection
