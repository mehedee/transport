@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3>List of Available Buses:</h3>
                <table class="table table-bordered">
                    <tr>
                        <th>Bus Name</th>
                        <th>Capacity</th>
                        <th>Current Route</th>
                    </tr>
                    @foreach( $all_buses as $bus)
                        <tr>
                            <td>
                                {{ $bus->bus_name }} <br>
                               ( <a href="{{ Route('buses.edit', $bus->id ) }}" class="small">Update Bus Name</a> )
                            </td>
                            <td>{{ $bus->capacity }}</td>
                            <td></td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-md-4 border-left">
                <h3>Add New Bus</h3>
                <hr>
                <form action="{{ route('buses.store') }}" method="post">
                    @csrf()
                    @include('bus.form')
                </form>
            </div>
        </div>

    </div>
@endsection
