
<div class="form-group">
        <label for="bus">Bus Name</label>
        <input type="text" class="form-control" name="bus_name" id="bus" value="{{ $bus->bus_name }}" required>
    </div>
<div class="form-group">
    <label for="capacity">Capacity</label>
    <input type="text" class="form-control" name="capacity" id="capacity" value="{{ $bus->capacity }}" required>
</div>
    <button type="submit" class="btn btn-primary">Submit</button>
