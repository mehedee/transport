@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3>Update :</h3>
                <form method="post" action="{{ route('fares.update', $fare->id)}}" >
                    @csrf()
                    {{ method_field('PUT')}}
                    @include('fare.form')
                </form>
            </div>
        </div>
    </div>
@endsection
