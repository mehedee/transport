@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h3>Create New Fare</h3>
                <hr>
                <form action="{{ route('fares.store') }}" method="post">
                    @csrf()
                    @include('fare.form')
                </form>
            </div>
        </div>

    </div>
@endsection
