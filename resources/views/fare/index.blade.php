@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Fare Table</h3>
            <hr>
            <a class="small" href="{{ route('fares.create') }}">Add New Fare</a>

            <table class="table table-bordered">
                <tr>
                    <th>Date</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Fare</th>
                </tr>
                @foreach($fares as $fare)
                    <tr>
                        <td>
                            {{ date('d M, y', strtotime($fare->date) )}}
                        </td>
                        <td>
                            {{ \App\Stops::find($fare->start_id)->stop_name }}
                        </td>
                        <td>
                            {{ \App\Stops::find($fare->destination_id)->stop_name }}
                        </td>
                        <td>
                            {{ $fare->fare_amount }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>
@endsection
