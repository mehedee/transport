<div class="form-group">
    <label for="date">Date</label>
    <input type="date" name="date" value="{{ $fare->date }}" class="form-control">
</div>
<div class="form-group">
    <label for="start_id">Starts From</label>
    <select name="start_id" id="" class="form-control">
        @foreach(\App\Stops::all() as $stop)
            <option value="{{ $stop->id }}"
                    @if($stop->id == $fare->id) selected="selected9" @endif>{{ $stop->stop_name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="destination_id">Destination</label>
    <select name="destination_id" id="" class="form-control">
        @foreach(\App\Stops::all() as $stop)
            <option value="{{ $stop->id }}">{{ $stop->stop_name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="fair_amount">Amount</label>
    <input type="text" name="fare_amount" value="{{ $fare->fare_amount }}" class="form-control">
</div>

<button type="submit" class="btn btn-primary">Submit</button>
