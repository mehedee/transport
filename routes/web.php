<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('routes','RouteController');
Route::resource('buses','BusController');
Route::resource('stops','StopController');
Route::resource('fares','FareController');
Route::resource('reservations','ReservationController');
Route::get('other', function(){
    return view('others');
})->name('other');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
